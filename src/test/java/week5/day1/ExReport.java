package week5.day1;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExReport {
	public static ExtentReports extent;
	public static String testCaseName, testCaseDescription, author, category, fileName;
	public static ExtentTest test;
	@BeforeSuite
	public void startResult()
	{
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	
	@BeforeMethod
	public void startTestCase()
	{
		test = extent.createTest(testCaseName, testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
	}

		
	public void reportStep(String desc, String status)
	{
		if(status.equalsIgnoreCase("pass"))
		{
			test.pass(desc);
		}
		if(status.equalsIgnoreCase("fail"))
		{
			test.fail(desc);
		}
	}
	@AfterSuite
	public void stopResult() {
		// TODO Auto-generated method stub
		extent.flush();
		

	}


}
