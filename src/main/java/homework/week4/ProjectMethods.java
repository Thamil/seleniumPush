package homework.week4;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdmethods.SeMethods;
import week6.day1.ReadFromExcel;

public class ProjectMethods extends SeMethods{
	@BeforeMethod
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException
	{
		Object[][] objects = ReadFromExcel.met(fileName);
		return objects;
	}
	@Parameters({"browser","appUrl","userName","password"})
	public void login(String browser, String appUrl, String userName, String password)
	{
		startApp(browser, appUrl);
		WebElement uName = locateElement("id","username");
		WebElement pWord = locateElement("id","password");
		type(uName, userName);
		type(pWord, password);
		WebElement LButton = locateElement("class", "decorativeSubmit");
		click(LButton);
		WebElement crmLink = locateElement("xpath","//div[@id='label']/a");
		click(crmLink);
		
	}
	/*@AfterMethod
public void close()
{
	closeAllBrowsers();
	}*/
}
