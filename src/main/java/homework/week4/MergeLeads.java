package homework.week4;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLeads {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByXPath("(//div[@id='label'])[1]//following::a").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//table[@name='ComboBox_partyIdFrom']//following::img)[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		System.out.println(windowHandles.size());
		List<String> listwindow = new ArrayList<String>();
		listwindow.addAll(windowHandles);
		String firstWindow = listwindow.get(0);
		String secondWindow = listwindow.get(1);
		driver.switchTo().window(secondWindow);
		driver.findElementByXPath("(//div[@class='x-form-element'])[1]/input").sendKeys("10027");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a")));
		
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a").click();
		driver.switchTo().window(firstWindow);
		driver.findElementByXPath("(//table[@name='ComboBox_partyIdFrom']//following::img)[2]").click();
		
		
		Set<String> windowHandles1 = driver.getWindowHandles();
		System.out.println(windowHandles1.size());
		List<String> listwindow1 = new ArrayList<String>();
		listwindow1.addAll(windowHandles1);
		String firstWindow1 = listwindow1.get(0);
		String secondWindow1 = listwindow1.get(1);
		driver.switchTo().window(secondWindow1);
		
		
		driver.findElementByXPath("(//div[@class='x-form-element'])[1]/input").sendKeys("10028");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		
		WebDriverWait wait1 = new WebDriverWait(driver,10);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a")));
		
		
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a").click();
		driver.switchTo().window(firstWindow1);
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//div[@class='x-form-element'])[18]/input").sendKeys("10027");
		driver.findElementByXPath("(//button[@class='x-btn-text'])[7]").click();
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("D://snaps/img.png");
		FileUtils.copyFile(src, dest);
		
		driver.close();
		
		

	}

}
