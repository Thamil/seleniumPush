package homework.week4;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidPassword {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the password which has atleast the below criteria...");
		System.out.println("1. Atleast 10 characters...");
		System.out.println("2. Only letters and digits...");
		System.out.println("3. Atleast 2 digits & 2 letters...");
		System.out.println("4. Atleast 1 capital letter...");
		Scanner scan = new Scanner(System.in);
		String text = scan.nextLine();
		String pattern = ".{10,} \\w \\d{2} [a-z A-Z] {2} [A-Z] {1}";
		Pattern p = Pattern.compile(pattern);
		Matcher matcher = p.matcher(text);
		System.out.println(matcher.matches());
		scan.close();
	}

}
