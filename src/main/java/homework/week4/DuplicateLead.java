package homework.week4;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class DuplicateLead extends ProjectMethods{

	@Test
	public void dLead()
	{
		login();
		WebElement Leads = locateElement("linktext", "Leads");
		click(Leads);
		WebElement fLeads = locateElement("linktext", "Find Leads");
		click(fLeads);
	}
}
