package homework.week4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class DeleteLeadSeMethod extends ProjectMethods{

	//@Test(dependsOnMethods="homework.week4.EditLead.editLead")
	@Test(groups="regression", dependsOnGroups="sanity")
	public void DelLead()
	{
		//login();
		WebElement Leads = locateElement("linktext", "Leads");
		click(Leads);
		WebElement fLeads = locateElement("linktext", "Find Leads");
		click(fLeads);
		WebElement Phone = locateElement("xpath", "//span[text()='Phone']");
		click(Phone);
		WebElement phoneTB = locateElement("xpath", "//input[@name='phoneNumber']");
		type(phoneTB, "12");
		WebElement findLead = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(findLead);
		
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));
		
		WebElement linkFL = locateElement("xpath", "((//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a)");
		String text = linkFL.getText();
		click(linkFL);
		
		WebElement bDelete = locateElement("linktext", "Delete");
		click(bDelete);
		
		WebElement fLeads1 = locateElement("linktext", "Find Leads");
		click(fLeads1);
		WebElement leadID = locateElement("xpath", "(//div[@class='x-form-element'])[18]/input");
		
		type(leadID, text);
		WebElement fleadsss = locateElement("xpath", "//button[text()='Find Leads']");
		click(fleadsss);
		
		closeBrowser();
		
		
		
		
		
		
		
	}
}
