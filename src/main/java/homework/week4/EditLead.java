package homework.week4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class EditLead extends ProjectMethods{

	//@Test(dependsOnMethods="week4.day2.CreateLead.Create")
	@Test(groups="sanity", dependsOnGroups="smoke")
	public void editLead()
	{
		
		WebElement Leads = locateElement("linktext", "Leads");
		click(Leads);
		WebElement fLeads = locateElement("linktext", "Find Leads");
		click(fLeads);
		WebElement fName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(fName, "Gopinath");
		WebElement FLead1 = locateElement("xpath", "((//td[@class='x-btn-right'])[7]//preceding-sibling::td)/em/button");
		click(FLead1);
		
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));
		WebElement fLink = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(fLink);
		
		verifyTitle("View Lead | opentaps CRM");
		
		WebElement bEdit = locateElement("linktext", "Edit");
		click(bEdit);
		WebElement tCName = locateElement("id", "updateLeadForm_companyName");
		tCName.clear();
		
		String updatedName = "Selenium Training"; 		
		type(tCName, updatedName);
		
		WebElement bUpdate = locateElement("class", "smallSubmit");
		click(bUpdate);
		
		WebElement gTitle = locateElement("id", "viewLead_companyName_sp");
		verifyPartialText(gTitle, updatedName);

		
		
		
		
		
		
		
		
		
		
	}
}
