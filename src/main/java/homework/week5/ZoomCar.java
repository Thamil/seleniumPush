package homework.week5;




/*import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.crypto.Mac;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

//import WdMethods.SeMethods;

public class ZoomCar extends SeMethods {

	@Test
	public void login() throws InterruptedException {

		startApp("Chrome","https://www.zoomcar.com/chennai/");
		WebElement ele1 = locateElement("class","search");
		ele1.click();
		WebElement ele2 = locateElement("xpath","(//div[@class='items'])[1]");
		ele2.click();
		WebElement ele3 = locateElement("xpath","//button[text()='Next']");
		ele3.click();
		WebElement ele4 = locateElement("xpath","(//div[@class='day'])[1]");
		ele4.click();
		WebElement ele5 = locateElement("xpath","//button[@class='proceed']");
		ele5.click();
		Thread.sleep(3000);
		WebElement ele6 = locateElement("xpath","(//div[@class='day'])[1]");
		ele6.click();
		WebElement ele7 = locateElement("xpath","//button[text()='Done']");
		ele7.click();
		Thread.sleep(3000);

		List<WebElement> carlist = driver.findElementsByXPath("//div[@class='car-listing' or @class='suggested-item']");
		int size = carlist.size();
		System.out.println(size);


		List<WebElement> carprice = driver.findElementsByXPath("//div[@class='price']");
		Set<Integer> pricedCar = new TreeSet<Integer>();


		for (WebElement eachCar : carprice) {
			System.out.println(eachCar.getText().replaceAll("\\D", ""));
			pricedCar.add(Integer.parseInt(eachCar.getText().replaceAll("\\D", "")));
		}

		List<Integer> priceCar = new ArrayList<Integer>(); 
		priceCar.addAll(pricedCar);
		int maxPrice = priceCar.get(priceCar.size()-1);

		System.out.println(maxPrice);

		String text = driver.findElementByXPath("(//div[contains(text(),'"+maxPrice+"')]/preceding::div[@class='car-name'])[last()]").getText();
		System.out.println(text);

		driver.findElementByXPath("(//div[contains(text(),'"+maxPrice+"')]/following::button)[1]").click();
		
		
	}
}*/








import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class ZoomCar extends SeMethods{
	@Test
	public void mMethod()
	{
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		WebElement tBox = locateElement("linktext", "Start your wonderful journey");
		click(tBox);
		WebElement area = locateElement("xpath", "//div[contains(text(),'Thuraipakkam')]");
		click(area);
		WebElement nButton = locateElement("xpath", "//button[@class='proceed']");
		click(nButton);
		WebElement startDate = locateElement("xpath", "//div[@class='day'][1]");
		click(startDate);
		WebElement nButton1 = locateElement("xpath", "//button[@class='proceed']");
		click(nButton1);
		
		WebElement endDate = locateElement("xpath", "//div[@class='day'][1]");
		click(endDate);
		WebElement nButton2 = locateElement("xpath", "//button[@class='proceed']");
		click(nButton2);
		
		Set<Integer> carPrice = new TreeSet<Integer>();
		
		
		List<WebElement> priceOfCars = driver.findElementsByXPath("//div[@class='price']");
		for (WebElement eachCar : priceOfCars) {
			
			System.out.println((eachCar.getText().replaceAll("\\D", "")));
			carPrice.add(Integer.parseInt(eachCar.getText().replaceAll("\\D", "")));
			
		}
		
		
		List<Integer> newPrice = new ArrayList<Integer>();
		newPrice.addAll(carPrice);
		System.out.println("--------XXXXXXXXX--------");
		System.out.println(newPrice.get(newPrice.size()-1));
		int maxPrice = newPrice.get(newPrice.size()-1);
		String carName = locateElement("xpath", "(//div[contains(text(),'"+maxPrice+"')]/preceding::div[@class='details'])[last()]").getText();
		System.out.println("Car name for the corresponding Max Price value...."+carName);
		
		WebElement bookButton = locateElement("xpath", "//div[contains(text(),'3,640')]//following::button");
		click(bookButton);
		

		
	}

}





/*import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

import WdMethods.ProjectMethods;
import WdMethods.SeMethods;

public class ZoomCar extends SeMethods {

		@BeforeMethod
	public void setData() {
		Testcasename = "BookACar";
		TestCasedescription ="Car booking";
		Author= "vickyjaya";
		Category = "Sanity";
	}

	@Test
	public void car() throws InterruptedException
	{
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement elez1 = locateElement("class","search");
		click(elez1);
		WebElement elez2 = locateElement("xpath","//div[contains(text(),'Thuraipakkam')]");
		click(elez2);
		WebElement elez3 = locateElement("class","proceed");
		click(elez3);

		WebElement elez4 = locateElement("xpath","(//div[@class='text']/following::div)[3]");
		Thread.sleep(3000);
		String text1= elez4.getText();
		System.out.println(text1);
		click(elez4);


		WebElement elez5 = locateElement("class","proceed");
		click(elez5);
		Thread.sleep(3000);
		WebElement elez6 = locateElement("xpath","(//div[@class='text']/following::div)[3]");
		Thread.sleep(3000);
		String text2 = elez6.getText();
		System.out.println(text2);

		verifyPartialText(elez6, text1);

		WebElement elez7 = locateElement("class","proceed");
		click(elez7);
		Thread.sleep(3000);

		List<WebElement> results = driver.findElementsByXPath("//div[@class='car-list-layout']/div");

		System.out.println(results.size());
		
		
		
		
		WebElement elez8 = locateElement("xpath","//div[@class='sort-bar car-sort-layout']//div");
		click(elez8);
		Thread.sleep(3000);
		WebElement elez9 = locateElement("xpath","(//div[@class='details']/h3)[1]");
		System.out.println(elez9.getText());
		
		WebElement elez10 = locateElement("xpath","(//button[@class='book-car'])[1]");
		click(elez10);
		
				
	}

}*/