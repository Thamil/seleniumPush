package homework.week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginLeafTaps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Thamil");
		
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select sel = new Select(src);
		sel.selectByIndex(2);
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Karthik");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Dear");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("450000");
		
		WebElement src1 = driver.findElementById("createLeadForm_industryEnumId");
		Select sel1 = new Select(src1);
		sel1.selectByIndex(2);
		
		WebElement src2 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select sel2 = new Select(src2);
		sel2.selectByValue("OWN_PARTNERSHIP");
		
		driver.findElementById("createLeadForm_sicCode").sendKeys("12145");
		driver.findElementById("createLeadForm_description").sendKeys("Hello... New to LeapTaps...");
		driver.findElementById("createLeadForm_importantNote").sendKeys("First day in LeapTap");
		driver.findElementById("createLeadForm_lastName").sendKeys("Anna");
		
		WebElement src3 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select sel3 = new Select(src3);
		sel3.selectByVisibleText("eCommerce Site Internal Campaign");
		
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Ann");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Operations");
		
		WebElement src4 = driver.findElementById("createLeadForm_currencyUomId");
		Select sel4 = new Select(src4);
		sel4.selectByVisibleText("BOB - Bolivian Boliviano");
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("34");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("$");
		
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("1");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("012");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("542");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("sdf@wer.com");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("044 25678495");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Kirk");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://leaftest.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Alagan");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("thistle street");
		driver.findElementById("createLeadForm_generalCity").sendKeys("edinburgh");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("621212");
		driver.findElementById("createLeadForm_generalPostalCodeExt");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Test");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("mid lothian");
		
		WebElement src5 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select sel5 = new Select(src5);
		sel5.selectByVisibleText("United States");
		
		
		WebElement src6 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select sel6 = new Select(src6);
		sel6.selectByVisibleText("Arizona");
		
		
		

	}

}
