package homework.week3.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SecondLargestNumberInAnArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] intArray = {20,340,21,879,92,21,474,83647,-200};
		List <Integer> lst = new ArrayList<Integer>();
		for (Integer integer : intArray) {
			lst.add(integer);
		}
		Collections.sort(lst);
		System.out.println("The second largest number in an array is ....");
		System.out.println(lst.get((lst.size()-2)));
			

	}

}
