package homework.week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class PrintAllValuesInDropDown {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		
		WebElement dropDown1 = driver.findElementById("userRegistrationForm:securityQ");
		Select dd1 = new Select(dropDown1);
		List<WebElement> options1 = dd1.getOptions();
		for (WebElement eachDD1 : options1) {
			System.out.println(eachDD1.getText());
		}
		
		WebElement dropDown2 = driver.findElementById("userRegistrationForm:prelan");
		Select dd2 = new Select(dropDown2);
		List<WebElement> options2 = dd2.getOptions();
		for (WebElement eachDD2 : options2) {
			System.out.println(eachDD2.getText());
		}
		
		WebElement dropDown3 = driver.findElementById("userRegistrationForm:countries");
		Select dd3 = new Select(dropDown3);
		dd3.selectByIndex(2);
		List<WebElement> options3 = dd3.getOptions();
		for (WebElement eachDD3 : options3) {
			System.out.println(eachDD3.getText());
		}
		
		

	}

}
