package homework.week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class IsChecked {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/checkbox.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//driver.findElementByXPath("(//label[@for=\"java\"])[1]").getAttribute();
		boolean b = driver.findElementByXPath("//label[@for=\"java\"]//following::input").isSelected();
		if(b)
		{
			System.out.println("Check box is checked");
		}
		else
		{
			System.out.println("Check box is not checked");
		}
		boolean c = driver.findElementByXPath("(//label[@for=\"java\"]//following::input)[6]").isSelected();
		if(c)
		{
			System.out.println("Check box is checked");
		}
		else
		{
			System.out.println("Check box is not checked");
		}

	}

}
