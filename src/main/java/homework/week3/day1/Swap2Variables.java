package homework.week3.day1;

public class Swap2Variables {
	static int a = 2;
	static int b = 3;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Before Swap.......");
		System.out.println("The value of a is " +a+ " and b is " +b);
		System.out.println("After Swap.....");
		a=a+b;
		b=a-b;
		a=a-b;
		
		System.out.println("The value of a is " +a+ " and b is " +b);

	}

}
