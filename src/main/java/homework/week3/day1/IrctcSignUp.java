package homework.week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcSignUp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Zqwpf");
		driver.findElementById("userRegistrationForm:password").sendKeys("Mar2018*");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Mar2018*");
		
		WebElement ele = driver.findElementById("userRegistrationForm:securityQ");
		Select sel = new Select(ele);
		sel.selectByVisibleText("Who was your Childhood hero?");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("rajni");
		
		WebElement ele1 = driver.findElementById("userRegistrationForm:prelan");
		Select sel1 = new Select(ele1);
		sel1.selectByValue("en");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Thamil");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Anna");
		driver.findElementByName("userRegistrationForm:gender").click();
		driver.findElementByName("userRegistrationForm:maritalStatus").click();
		
		WebElement ele2 = driver.findElementById("userRegistrationForm:dobDay");
		Select sel2 = new Select(ele2);
		sel2.selectByValue("06");
		
		WebElement ele3 = driver.findElementById("userRegistrationForm:dobMonth");
		Select sel3 = new Select(ele3);
		sel3.selectByVisibleText("MAR");
		
		WebElement ele4 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select sel4 = new Select(ele4);
		sel4.selectByIndex(5);
		
		WebElement ele5 = driver.findElementById("userRegistrationForm:occupation");
		Select sel5 = new Select(ele5);
		sel5.selectByIndex(5);
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("1258 7546 2148");
		driver.findElementById("userRegistrationForm:idno").sendKeys("PJRNE5487G");
		
		
		WebElement ele6 = driver.findElementById("userRegistrationForm:countries");
		Select sel6 = new Select(ele6);
		sel6.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("fdg@ert.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("6954781564");
		
		WebElement ele7 = driver.findElementById("userRegistrationForm:nationalityId");
		Select sel7 = new Select(ele7);
		sel7.selectByVisibleText("India");
		
		
		driver.findElementById("userRegistrationForm:address").sendKeys("25");
		driver.findElementById("userRegistrationForm:street").sendKeys("thistle street");
		driver.findElementById("userRegistrationForm:area").sendKeys("Henderson row");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("603103",Keys.TAB);
		
		WebElement ele8 = driver.findElementById("userRegistrationForm:cityName");
		Select sel8 = new Select(ele8);
		sel8.selectByValue("Kanchipuram");
		
		WebElement ele9 = driver.findElementById("userRegistrationForm:postofficeName");
		Select sel9 = new Select(ele9);
		sel9.selectByValue("Navalur B.O");
				
		driver.findElementById("userRegistrationForm:landline").sendKeys("6547912548");
		
		
		
		
		
		
		
		

	}

}
