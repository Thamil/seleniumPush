package homework.week3.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditLeads {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()=\"Leads\"]").click();
		driver.findElementByXPath("//a[text()=\"Find Leads\"]").click();
		driver.findElementByXPath("(//div[@id=\"findLeads\"]//following::input)[2]").sendKeys("s");
		driver.findElementByXPath("((//div[@class=\"x-panel-bl x-panel-nofooter\"])[1]//preceding::button)[7]").click();
		WebDriverWait wait = new WebDriverWait(driver,10);
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"))).click();
		
		System.out.println(driver.getTitle());
		driver.findElementByXPath("//a[@class=\"subMenuButton\"][3]").click();
		driver.findElementByXPath("//input[@id=\"updateLeadForm_companyName\"]").clear();
		driver.findElementByXPath("//input[@id=\"updateLeadForm_companyName\"]").sendKeys("Bsam");
		driver.findElementByXPath("(//input[@class=\"smallSubmit\"])[1]").click();
		driver.findElementById("viewLead_companyName_sp").getText();
		driver.close();
		
		
		
		

	}

}
