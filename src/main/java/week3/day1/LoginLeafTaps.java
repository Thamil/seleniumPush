package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginLeafTaps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		WebElement obj = driver.findElementById("createLeadForm_dataSourceId");
		Select sel = new Select(obj);
		sel.selectByVisibleText("Conference");
		
		WebElement obj1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select sel1 = new Select(obj1);
		sel1.selectByValue("CATRQ_CARNDRIVER");
		
		List <WebElement> l = sel1.getOptions();
		for (WebElement eachoption : l) {
			System.out.println(eachoption.getText());
		}
		/*driver.findElementById("createLeadForm_companyName").sendKeys("Accenture");
		driver.findElementById("createLeadForm_firstName").sendKeys("TestUser");
		driver.findElementById("createLeadForm_lastName").sendKeys("UserTest");
		driver.findElementByName("submitButton").click();*/
		
		
		
		

	}

}
