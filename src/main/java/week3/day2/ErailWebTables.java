package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailWebTables {

	public static void main(String[] args) {
		//TESTING FOR PUSH AND PULL FROM GIT
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();		
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		driver.findElementById("buttonFromTo").click();
		boolean val = driver.findElementById("chkSelectDateOnly").isSelected();
		if(val)
		{
			driver.findElementById("chkSelectDateOnly").click();
		}
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> tr = table.findElements(By.tagName("tr"));
		System.out.println(tr.size());
		
		WebElement FirstRow = tr.get(0);
		List<WebElement> td = FirstRow.findElements(By.tagName("td"));
		String text = td.get(1).getText();
		System.out.println(text);
		
		for(WebElement eachTr : tr)
		{
			String tet = eachTr.findElements(By.tagName("td")).get(1).getText();
			
			System.out.println(tet);
		}
		

	}

}
