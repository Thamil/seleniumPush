package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import homework.week4.ProjectMethods;
import wdmethods.SeMethods;
import week6.day1.ReadFromExcel;

public class CreateLead extends ProjectMethods{
	@BeforeClass
	public void bClass()
	{
		testCaseName = "Tc001";
		testCaseDescription = "This test case is to create lead....";
	    author = "Thamil";
		category = "Smoke";
		fileName = "createlead";
		
	}
	
	//@Test(invocationCount=2,invocationTimeOut=30000)
	//@Test(groups="smoke")
	/*@DataProvider(name="CreateLead")
	public Object[][] getData()
	{
		Object[][] data = new Object[2][3];
		data[0][0] = "cts";
		data[0][1] = "thamil";
		data[0][2] = "alagan";
		
		data[1][0] = "wipro";
		data[1][1] = "karthik";
		data[1][2] = "rudhran";
		
		return data;
	}*/
	
	@Test(dataProvider="fetchData")
	
	
	public void Create(String compName, String firstName, String lastName)
	{
		/*startApp("chrome", "");
		WebElement uName = locateElement("id","username");
		WebElement pWord = locateElement("id","password");
		type(uName, userName);
		type(pWord, password);
		WebElement LButton = locateElement("class", "decorativeSubmit");
		click(LButton);
		WebElement crmLink = locateElement("xpath","//div[@id='label']/a");
		click(crmLink);*/
		WebElement Leads = locateElement("linktext", "Leads");
		click(Leads);
		WebElement CreateLead = locateElement("linktext", "Create Lead");
		click(CreateLead);
		WebElement cName = locateElement("id", "createLeadForm_companyName");
		type(cName, compName);
		WebElement fName = locateElement("id", "createLeadForm_firstName");
		type(fName, firstName);
		WebElement lName = locateElement("id", "createLeadForm_lastName");
		type(lName, lastName);
		WebElement dDown = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(dDown,"Direct Mail");
		WebElement dDown1 = locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(dDown1,3);
		WebElement dDown2 = locateElement("id","createLeadForm_ownershipEnumId");
		selectDropDownUsingValue(dDown2,"OWN_CCORP");
		WebElement CreateLeadButton = locateElement("class", "smallSubmit");
		click(CreateLeadButton);
	}

}
