package week4.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class MergeLeads extends SeMethods {
	@Test(enabled=false)

	public void merge()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement uName = locateElement("id","username");
		WebElement pWord = locateElement("id","password");
		type(uName, "DemoSalesManager");
		type(pWord, "crmsfa");
		WebElement LButton = locateElement("class", "decorativeSubmit");
		click(LButton);
		WebElement crmLink = locateElement("xpath","//div[@id='label']/a");
		click(crmLink);
		WebElement Leads = locateElement("linktext", "Leads");
		click(Leads);
		WebElement mer = locateElement("linktext", "Merge Leads");
		click(mer);
		WebElement img1 = locateElement("xpath", "//input[@id='partyIdFrom']//following::a/img");
		click(img1);
		List<String> wList = WindowHandles();
		switchToWindow(wList, 1);
		System.out.println("Window 1 "+wList);
		WebElement w2Lead = locateElement("name", "id");
		type(w2Lead, "10201");
		WebElement w2fLead = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
		click(w2fLead);
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")));
		WebElement w2LLead = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(w2LLead);

		List<String> wList1 = WindowHandles();
		switchToWindow(wList1, 0);
		System.out.println("Window 0 "+wList1);
		
		WebElement img2 = locateElement("xpath", "(//input[@id='partyIdTo']//following::a/img)[1]");
		click(img2);
		List<String> wList2 = WindowHandles();
		switchToWindow(wList2, 1);
		System.out.println("Window 0 "+wList2);
	
		WebElement w2Lead1 = locateElement("name", "id");
		type(w2Lead1, "10202");
		WebElement w2fLead1 = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
		click(w2fLead1);
		WebDriverWait wait1 = new WebDriverWait(driver,10);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")));
		WebElement w2LLead1 = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(w2LLead1);
		
		switchToWindow(wList2, 0);
		
		WebElement mButton = locateElement("linktext", "Merge");
		clickWithNoSnap(mButton);
		
		acceptAlert();
		
		WebElement fLeadsL = locateElement("linktext", "Find Leads");
		click(fLeadsL);
		
		WebElement BLeadId = locateElement("name", "id");
		type(BLeadId, "10201");
		
		WebElement fLeads3 = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(fLeads3);
		
		
	}
}
