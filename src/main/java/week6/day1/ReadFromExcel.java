package week6.day1;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFromExcel {

	//convert from main method to static method
	//public static void main(String[] args) throws IOException
	public static Object[][] met(String fileName) throws IOException{
		// TODO Auto-generated method stub
		XSSFWorkbook wb = new XSSFWorkbook("./testdata/"+fileName+".xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] obj = new Object[lastRowNum][lastCellNum];
		System.out.println("row "+lastRowNum+" lastCellNum "+lastCellNum);
		for (int j = 1; j <= lastRowNum; j++) {
			XSSFRow row = sheet.getRow(j);
			
			for (int i = 0; i < lastCellNum; i++) {
				XSSFCell cell = row.getCell(i);
				String text = cell.getStringCellValue();
				obj[j-1][i] = text;
				System.out.println(text);
			} 
		}
		//wb.close();
		return obj;
		
		

	}

}
